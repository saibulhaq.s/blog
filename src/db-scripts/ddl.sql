/*
drop table t_blog;
drop table m_blog;
drop table m_account;
drop table m_role;
drop table m_mode;
drop table m_status;
*/
================================================================================
CREATE TABLE `m_blog` (
   id int not null auto_increment primary key,
  `name` VARCHAR(300) NOT NULL,
  `body` TEXT NULL,
  `mode_id` int NOT NULL,
  `status_id` int NOT NULL,
  `created_by` INT NOT NULL,
  `created_on` timestamp default now(),
  `updated_by` INT NULL,
  `updated_on` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	
    FOREIGN KEY (mode_id)  REFERENCES m_mode(id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT,
   
    FOREIGN KEY (status_id)  REFERENCES m_status(id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT

);
================================================================================
CREATE TABLE t_blog(
   id int not null auto_increment primary key,
   blog_id int not null,
   body text not null,
  `created_by` INT NOT NULL,
  `created_on` timestamp default now(),
  `updated_by` INT NULL,
  `updated_on` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   
   FOREIGN KEY (blog_id) REFERENCES m_blog(id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT
);
  ================================================================================
  CREATE TABLE `m_role` (
  id int not null auto_increment primary key,
  `name` VARCHAR(50) NOT NULL,
  `created_by` INT NOT NULL,
  `created_on` timestamp default now(),
  `updated_by` INT NULL,
  `updated_on` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  );
  ================================================================================
  CREATE TABLE `m_mode` (
  id int not null auto_increment primary key,
  `name` VARCHAR(50) NOT NULL,
  `created_by` INT NOT NULL,
  `created_on` timestamp default now(),
  `updated_by` INT NULL,
  `updated_on` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  );
  ================================================================================
  CREATE TABLE `m_status` (
  id int not null auto_increment primary key,
  `name` VARCHAR(50) NOT NULL,
  `created_by` INT NOT NULL,
  `created_on` timestamp default now(),
  `updated_by` INT NULL,
  `updated_on` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  );
  ================================================================================
CREATE TABLE `m_account` (
  id int not null auto_increment primary key,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `role_id` INT NULL,
  `user_name` VARCHAR(45) NULL,
  `pwd` TEXT NULL,
  `status_id` INT NULL,
  `email` TEXT NULL,
  `created_by` INT NOT NULL,
  `created_on` timestamp default now(),
  `updated_by` INT NULL,
  `updated_on` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY fk_cat(role_id) REFERENCES m_role(id)
   ON UPDATE CASCADE
   ON DELETE RESTRICT);
    
