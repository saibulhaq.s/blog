/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saibul.blog.auth;

import com.saibul.blog.model.Account;
import com.saibul.blog.repository.AccountRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import static java.util.Collections.emptyList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Saibul.Haq
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    public UserDetailsServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        System.out.println("pwd :" + new BCryptPasswordEncoder().encode("Welcome01"));
        System.out.println("username : " + username);
        List<Account> accountAL = accountRepository.findByUserName(username);
        System.out.println("in acccount...");
        System.out.println("accountAL :" + accountAL);
        System.out.println("accountAL.size() :" + accountAL.size());
        System.out.println("accountAL.get(0) :" + accountAL.get(0).getUserName());
        System.out.println("accountAL.get(0) :" + accountAL.get(0).getPassword());
        if (accountAL == null || accountAL.size() == 0) {
            throw new UsernameNotFoundException(username);
        }
        return new User(accountAL.get(0).getUserName(), accountAL.get(0).getPassword(), emptyList());
    }
}
