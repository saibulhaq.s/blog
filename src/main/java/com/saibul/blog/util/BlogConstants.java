package com.saibul.blog.util;

public class BlogConstants {

    public static final String SERVICE_GROUP_NAME = "blogs";
    public static final String SERVICE_BASE_URL = "/saibul/apis";

    public static final String STATUS_SUCCESS = "SUCCESS";
    public static final String STATUS_FAIL = "FAIL";
    public static final String STATUS_ERROR = "ERROR";

    public static final String API_VERSION = "v1";

    public static final String API_NAME_GET = "GetBlog";
    public static final String API_NAME_CREATE = "CreateBlog";
    public static final String API_NAME_PUT = "UpdateBlog";
    public static final String API_NAME_DELETE = "DeleteBlog";

    public static final int SUCCESS_STATUS_CODE = 200;
    public static final int BAD_REQUEST_STATUS_CODE = 400;
    public static final int UNAUTHORIZED_STATUS_CODE = 401;
    public static final int FORBIDDEN_STATUS_CODE = 403;
    public static final int SERVER_ERROR_STATUS_CODE = 500;

}
