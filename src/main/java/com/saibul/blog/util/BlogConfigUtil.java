package com.saibul.blog.util;

import com.saibul.blog.model.Blog;
import com.saibul.blog.vo.BlogResponseVo;
import com.saibul.blog.vo.Summary;

import java.util.ArrayList;
import java.util.List;



public class BlogConfigUtil {

    public BlogResponseVo buildBlogResponse(Blog blog) {
        BlogResponseVo response = new BlogResponseVo();
        Summary summary = new Summary();
        summary.setApiVersion(BlogConstants.API_VERSION);
        summary.setApiName(BlogConstants.API_NAME_GET);
        response.setSummary(summary);
        response.setStatus(BlogConstants.STATUS_SUCCESS);

        List<Blog> list = new ArrayList<Blog>();
        list.add(blog);
        response.setBlogs(list);

        return response;
    }

    public String prefixQueryString(String query) {
        if (query != null) {
            query = "%".concat(query).concat("%");
        }
        return query;
    }
}
