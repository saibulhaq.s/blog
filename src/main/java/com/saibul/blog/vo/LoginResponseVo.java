package com.saibul.blog.vo;

public class LoginResponseVo {

    private String status = null;
    private String token = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
