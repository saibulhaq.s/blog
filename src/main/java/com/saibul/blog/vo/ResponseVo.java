package com.saibul.blog.vo;

public class ResponseVo {

    private Summary summary;
    private ErrorVo error;
    private String status;

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public ErrorVo getError() {
        return error;
    }

    public void setError(ErrorVo error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
