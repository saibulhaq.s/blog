package com.saibul.blog.vo;

import java.util.List;

import com.saibul.blog.model.Blog;

public class BlogResponseVo extends ResponseVo {
    private List<Blog> blogs;

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }
}
