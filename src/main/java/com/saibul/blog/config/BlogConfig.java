package com.saibul.blog.config;

import com.saibul.blog.mapper.BlogMapper;
import com.saibul.blog.util.BlogConfigUtil;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The Class DataContextProductServicesConfig.
 */
@Configuration
public class BlogConfig {

    /**
     * Gets the DataContextProductServicesConfig util.
     *
     * @return the DataContextProductServicesConfig util
     */
    @Bean
    public BlogConfigUtil getProductUtil() {
        return new BlogConfigUtil();
    }

    /**
     * Gets the DataContextProductService mapper.
     *
     * @return the DataContextProductService mapper
     */
    @Bean
    public BlogMapper getProductMapper() {
        return new BlogMapper();
    }
}
