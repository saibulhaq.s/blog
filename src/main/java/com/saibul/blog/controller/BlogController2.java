package com.saibul.blog.controller;

import com.saibul.blog.exception.BlogException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.saibul.blog.service.BlogService;
import com.saibul.blog.util.BlogConstants;
import com.saibul.blog.vo.BlogResponseVo;

@RestController
public class BlogController2 {

    @Autowired
    private BlogService service;

    @ApiOperation(value = "Get Blog by Id2", nickname = "Get Blog2", produces = "application/json")
    @ApiResponses(value = {
        @ApiResponse(code = BlogConstants.SUCCESS_STATUS_CODE, message = "Success", response = BlogController.class)
        ,
			@ApiResponse(code = BlogConstants.UNAUTHORIZED_STATUS_CODE, message = "User Unauthorized")
        ,
			@ApiResponse(code = BlogConstants.SERVER_ERROR_STATUS_CODE, message = "Internal Server Error")})
    @RequestMapping(path = BlogConstants.SERVICE_BASE_URL + "/blog2/{id}", method = RequestMethod.GET)
    public final @ResponseBody
    ResponseEntity<BlogResponseVo> getProduct(@PathVariable final Integer id)
            throws BlogException {
        BlogResponseVo productVo = null;
        try {

            productVo = service.getBlogById(id);
        }
        catch (BlogException exception) {
        }
        return new ResponseEntity<BlogResponseVo>(productVo, HttpStatus.OK);
    }

}
