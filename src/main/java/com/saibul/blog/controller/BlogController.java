package com.saibul.blog.controller;

import com.saibul.blog.exception.BlogException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.saibul.blog.service.BlogService;
import com.saibul.blog.util.BlogConstants;
import com.saibul.blog.vo.BlogRequestVo;
import com.saibul.blog.vo.BlogResponseVo;
import com.saibul.blog.vo.LoginResponseVo;
import com.saibul.blog.vo.LoginRequestVo;
import com.saibul.blog.vo.SaveUserRequestVo;

@RestController
public class BlogController {

    @Autowired
    private BlogService service;

    @ApiOperation(value = "Get Blog by Id", nickname = "Get Blog", produces = "application/json")
    @ApiResponses(value = {
        @ApiResponse(code = BlogConstants.SUCCESS_STATUS_CODE, message = "Success", response = BlogController.class)
        ,
			@ApiResponse(code = BlogConstants.UNAUTHORIZED_STATUS_CODE, message = "User Unauthorized")
        ,
			@ApiResponse(code = BlogConstants.SERVER_ERROR_STATUS_CODE, message = "Internal Server Error")})
    @RequestMapping(path = BlogConstants.SERVICE_BASE_URL + "/blog/{id}", method = RequestMethod.GET)
    public final @ResponseBody
    ResponseEntity<BlogResponseVo> getProduct(@PathVariable final Integer id)
            throws BlogException {
        BlogResponseVo responseVo = null;
        try {

            responseVo = service.getBlogById(id);
        }
        catch (BlogException exception) {
        }
        return new ResponseEntity<BlogResponseVo>(responseVo, HttpStatus.OK);
    }

    @ApiOperation(value = "Save a Blog", nickname = "Save Blog", produces = "application/json")
    @ApiResponses(value = {
        @ApiResponse(code = BlogConstants.SUCCESS_STATUS_CODE, message = "Success", response = BlogController.class)
        ,
			@ApiResponse(code = BlogConstants.UNAUTHORIZED_STATUS_CODE, message = "User Unauthorized")
        ,
			@ApiResponse(code = BlogConstants.SERVER_ERROR_STATUS_CODE, message = "Internal Server Error")})
    @RequestMapping(path = BlogConstants.SERVICE_BASE_URL + "/blog", method = RequestMethod.POST)
    public final @ResponseBody
    ResponseEntity<BlogResponseVo> saveBlog(@RequestBody BlogRequestVo requestVo)
            throws BlogException {
        BlogResponseVo responseVo = null;
        try {

            responseVo = service.saveBlog(requestVo);
        }
        catch (BlogException exception) {
        }
        return new ResponseEntity<BlogResponseVo>(responseVo, HttpStatus.OK);
    }

    @ApiOperation(value = "Update a Blog", nickname = "Update Blog", produces = "application/json")
    @ApiResponses(value = {
        @ApiResponse(code = BlogConstants.SUCCESS_STATUS_CODE, message = "Success", response = BlogController.class)
        ,
			@ApiResponse(code = BlogConstants.UNAUTHORIZED_STATUS_CODE, message = "User Unauthorized")
        ,
			@ApiResponse(code = BlogConstants.SERVER_ERROR_STATUS_CODE, message = "Internal Server Error")})
    @RequestMapping(path = BlogConstants.SERVICE_BASE_URL + "/blog", method = RequestMethod.PUT)
    public final @ResponseBody
    ResponseEntity<BlogResponseVo> updateBlog(@RequestBody BlogRequestVo requestVo)
            throws BlogException {
        BlogResponseVo responseVo = null;
        try {

            responseVo = service.updateBlog(requestVo);
        }
        catch (BlogException exception) {
        }
        return new ResponseEntity<BlogResponseVo>(responseVo, HttpStatus.OK);
    }
    
    
    @ApiOperation(value = "Save User", nickname = "Save User", produces = "application/json")
    @ApiResponses(value = {
        @ApiResponse(code = BlogConstants.SUCCESS_STATUS_CODE, message = "Success", response = BlogController.class)
        ,
			@ApiResponse(code = BlogConstants.UNAUTHORIZED_STATUS_CODE, message = "User Unauthorized")
        ,
			@ApiResponse(code = BlogConstants.SERVER_ERROR_STATUS_CODE, message = "Internal Server Error")})
    @RequestMapping(path = BlogConstants.SERVICE_BASE_URL + "/save-user", method = RequestMethod.POST)
    public final @ResponseBody
    ResponseEntity<String> loginUser(@RequestBody SaveUserRequestVo requestVo)
            throws BlogException {
        String responseVo = null;
        try {
            responseVo = service.saveUser(requestVo);
        }
        catch (BlogException exception) {
        }
        return new ResponseEntity<String>(responseVo, HttpStatus.OK);
    }

    @ApiOperation(value = "Login User", nickname = "Login User", produces = "application/json")
    @ApiResponses(value = {
        @ApiResponse(code = BlogConstants.SUCCESS_STATUS_CODE, message = "Success", response = BlogController.class)
        ,
			@ApiResponse(code = BlogConstants.UNAUTHORIZED_STATUS_CODE, message = "User Unauthorized")
        ,
			@ApiResponse(code = BlogConstants.SERVER_ERROR_STATUS_CODE, message = "Internal Server Error")})
    @RequestMapping(path = BlogConstants.SERVICE_BASE_URL + "/login", method = RequestMethod.POST)
    public final @ResponseBody
    ResponseEntity<LoginResponseVo> loginUser(@RequestBody LoginRequestVo requestVo)
            throws BlogException {
        LoginResponseVo responseVo = null;
        try {
            responseVo = service.loginUser(requestVo.getUserName(), requestVo.getPassword());
        }
        catch (BlogException exception) {
        }
        return new ResponseEntity<LoginResponseVo>(responseVo, HttpStatus.OK);
    }
}
