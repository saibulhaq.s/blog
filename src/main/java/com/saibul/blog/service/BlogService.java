package com.saibul.blog.service;

import com.saibul.blog.exception.BlogException;
import com.saibul.blog.model.Account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saibul.blog.model.Blog;
import com.saibul.blog.model.Mode;
import com.saibul.blog.model.Status;
import com.saibul.blog.model.Role;
import com.saibul.blog.repository.AccountRepository;
import com.saibul.blog.util.BlogConfigUtil;
import com.saibul.blog.vo.BlogRequestVo;
import com.saibul.blog.vo.BlogResponseVo;
import com.saibul.blog.repository.BlogRepository;
import com.saibul.blog.repository.ModeRepository;
import com.saibul.blog.repository.RoleRepository;
import com.saibul.blog.repository.StatusRepository;
import com.saibul.blog.vo.LoginResponseVo;
import com.saibul.blog.vo.SaveUserRequestVo;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Service
@Transactional
public class BlogService {

    @Autowired
    private BlogRepository blogRepository;
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    private ModeRepository modeRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Autowired
    private BlogConfigUtil util;

    public BlogResponseVo saveBlog(BlogRequestVo requestVo) throws BlogException {

        Status status = statusRepository.findById(requestVo.getStatus()).get();
        Mode mode = modeRepository.findById(requestVo.getMode()).get();

        Blog blog = new Blog();
        blog.setName(requestVo.getName());
        blog.setBody(requestVo.getBody());
        blog.setStatus(status);
        blog.setMode(mode);
        blog.setCreatedBy(requestVo.getCreatedBy());
        blog.setUpdatedBy(requestVo.getUpdatedBy());

//        System.out.println( BlogUtil.getJsonString(blog));
        blog = blogRepository.save(blog);
        return util.buildBlogResponse(blog);
    }

    public BlogResponseVo updateBlog(BlogRequestVo requestVo) throws BlogException {
        Status status = statusRepository.findById(requestVo.getStatus()).get();
        Mode mode = modeRepository.findById(requestVo.getMode()).get();

        String roleName = "admin";
        if (mode.getName().equalsIgnoreCase("publish") && "admin".equalsIgnoreCase(roleName)) {

        }
        Blog blog = new Blog();
        blog.setName(requestVo.getName());
        blog.setBody(requestVo.getBody());
        blog.setStatus(status);
        blog.setMode(mode);
        blog.setCreatedBy(requestVo.getCreatedBy());
        blog.setUpdatedBy(requestVo.getUpdatedBy());

//        System.out.println(BlogUtil.getJsonString(blog));
        blog = blogRepository.save(blog);

        return util.buildBlogResponse(blog);
    }

    public LoginResponseVo loginUser(String userName, String pwd) throws BlogException {
        LoginResponseVo responseVo = null;

        String encPwd = new BCryptPasswordEncoder().encode(pwd);
        System.out.println("userName :" + userName);
        System.out.println("pwd :" + pwd);
        System.out.println("encPwd :" + encPwd);
        return responseVo;
    }

    public String saveUser(SaveUserRequestVo requestVo) throws BlogException {

        Status status = statusRepository.findById(requestVo.getStatus()).get();
        Role role = roleRepository.findById(requestVo.getRole()).get();

        String encPwd = bCryptPasswordEncoder.encode(requestVo.getPassword());
        
        Account account = new Account();
        account.setFirstName(requestVo.getFirstName());
        account.setLastName(requestVo.getLastName());
        account.setUserName(requestVo.getUserName());
        account.setPassword(encPwd);
        account.setRole(role);
        account.setStatus(status);
        account.setEmail(requestVo.getEmail());
        
        account.setCreatedBy(1);
        account.setUpdatedBy(1);
        accountRepository.save(account);
        return "successsssss";
    }

    public BlogResponseVo getBlogById(Integer id) throws BlogException {
        Blog product = blogRepository.findById(id).get();

        // System.out.println(BlogUtil.getJsonString(product));
        return util.buildBlogResponse(product);
    }
}
