package com.saibul.blog.repository;

import com.saibul.blog.model.Account;
import java.util.List;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@EntityScan(basePackages = {"com.saibul.blog.model"})
@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {

    List<Account> findByUserName(String userName);
}
