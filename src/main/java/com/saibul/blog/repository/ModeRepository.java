package com.saibul.blog.repository;

import java.util.Optional;

import com.saibul.blog.model.Mode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan(basePackages = {"com.saibul.blog.model"})
@Repository
public interface ModeRepository extends CrudRepository<Mode, Integer> {

    @Override
    Optional<Mode> findById(Integer id);

    @Override
    Iterable<Mode> findAll();
}
