package com.saibul.blog.repository;

import java.util.Optional;

import com.saibul.blog.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan(basePackages = {"com.saibul.blog.model"})
@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {

    @Override
    Optional<Role> findById(Integer id);

    @Override
    Iterable<Role> findAll();
}
