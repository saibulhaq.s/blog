package com.saibul.blog.repository;

import java.util.Optional;

import com.saibul.blog.model.Status;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan(basePackages = {"com.saibul.blog.model"})
@Repository
public interface StatusRepository extends CrudRepository<Status, Integer> {

    @Override
    Optional<Status> findById(Integer id);

    @Override
    Iterable<Status> findAll();
}
