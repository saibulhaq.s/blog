package com.saibul.blog.repository;

import java.util.Optional;

import com.saibul.blog.model.Blog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan(basePackages = {"com.saibul.blog.model"})
@Repository
public interface BlogRepository extends CrudRepository<Blog, Integer> {

    @Override
    Optional<Blog> findById(Integer id);
}
